### 破坏电力设备罪、破坏易燃易爆设备罪
第一百一十八条 破坏电力、燃气或者其他易燃易爆设备，危害公共安全，尚未造成严重后果的，处三年以上十年以下有期徒刑。

#### 最高人民法院、最高人民检察院、公安部关于办理盗窃油气、破坏油气设备等刑事案件适用法律若干问题的意见

法发〔2018〕18号　2018年9月28日

一、关于危害公共安全的认定
在实施盗窃油气等行为过程中，破坏正在使用的油气设备，具有下列情形之一的，应当认定为刑法第一百一十八条规定的“危害公共安全”：
（一）采用切割、打孔、撬砸、拆卸手段的，但是明显未危害公共安全的除外；
（二）采用开、关等手段，足以引发火灾、爆炸等危险的。

#### 最高人民法院关于审理破坏电力设备刑事案件具体应用法律若干问题的解释
（2007 年8 月21日 法释〔2007〕15号）
第三条 盗窃电力设备，危害公共安全，但不构成盗窃罪的，以破坏电力设备罪定罪处罚；同时构成盗窃罪和破坏电力设备罪的，依照刑法处罚较重的规定定罪处罚。
盗窃电力设备，没有危及公共安全，但应当追究刑事责任的，可以根据案件的不同情况，按照盗窃罪等犯罪处理。
第四条 本解释所称电力设备，是指处于运行、应急等使用中的电力设备；已经通电使用，只是由于枯水季节或电力不足等原因暂停使用的电力设备；已经交付使用但尚未通电的电力设备。不包括尚未安装完毕，或者已经安装完毕但尚未交付使用的电力设备。

#### 最高人民法院、最高人民检察院关于办理盗窃油气、破坏油气设备等刑事案件具体应用法律若干问题的解释
（2007年1月19日  法释〔2007〕3号）

最高人民法院、最高人民检察院关于办理盗窃油气、破坏油气设备等刑事案件具体应用法律若干问题的解释
（2007年1月19日  法释〔2007〕3号）
为维护油气的生产、运输安全，依法惩治盗窃油气、破坏油气设备等犯罪，根据刑法有关规定，现就办理这类刑事案件具体应用法律的若干问题解释如下：
第一条 在实施盗窃油气等行为过程中，采用切割、打孔、撬砸、拆卸、开关等手段破坏正在使用的油气设备的，属于刑法第一百一十八条规定的“破坏燃气或者其他易燃易爆设备”的行为；危害公共安全，尚未造成严重后果的，依照刑法第一百一十八条的规定定罪处罚。
第二条 实施本解释第一条规定的行为，具有下列情形之一的，属于刑法第一百一十九条第一款规定的“造成严重后果”，依照刑法第一百一十九条第一款的规定定罪处罚：
（一）造成一人以上死亡、三人以上重伤或者十人以上轻伤的；
（二）造成井喷或者重大环境污染事故的；
（三）造成直接经济损失数额在五十万元以上的；
（四）造成其他严重后果的。
…第四条 盗窃油气同时构成盗窃罪和破坏易燃易爆设备罪的，依照刑法处罚较重的规定定罪处罚。
…第八条 本解释所称的“油气”，是指石油、天然气。其中，石油包括原油、成品油；天然气包括煤层气。
本解释所称“油气设备”，是指用于石油、天然气生产、储存、运输等易燃易爆设备。


