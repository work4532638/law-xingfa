115.  放火罪、决水罪、爆炸罪、投放危险物质罪、以危险方法危害公共安全罪(造成严重后果)，失火罪、过失决水罪、过失爆炸罪、过失投放危险物质罪、过失以危险方法危害公共安全罪
第一百一十五条 放火、决水、爆炸、投毒或者以其他危险方法致人重伤、死亡或者使公私财产遭受重大损失的，处十年以上有期徒刑、无期徒刑或者死刑。
过失犯前款罪的，处三年以上七年以下有期徒刑；情节较轻的，处三年以下有期徒刑或者拘役。

最高人民法院最高人民检察院公安部关于办理涉窨井盖相关刑事案件的指导意见
高检发〔2020〕3号 2020年3月16日　全文在第 46 页
二、盗窃、破坏人员密集往来的非机动车道、人行道以及车站、码头、公园、广场、学校、商业中心、厂区、社区、院落等生产生活、人员聚集场所的窨井盖，足以危害公共安全，尚未造成严重后果的，依照刑法第一百一十四条的规定，以以危险方法危害公共安全罪定罪处罚；致人重伤、死亡或者使公私财产遭受重大损失的，依照刑法第一百一十五条第一款的规定处罚。
过失致人重伤、死亡或者使公私财产遭受重大损失的，依照刑法第一百一十五条第二款的规定，以过失以危险方法危害公共安全罪定罪处罚。

最高人民法院最高人民检察院公安部司法部关于依法惩治妨害新型冠状病毒感染肺炎疫情防控违法犯罪的意见 
法发〔2020〕7号　自2020.2.6起施行 已被《关于适应新阶段疫情防治政策调整依法妥善办理相关刑事案件的通知》废止，2023年1月8日起不再执行。
二、准确适用法律，依法严惩妨害疫情防控的各类违法犯罪
（一）依法严惩抗拒疫情防控措施犯罪。故意传播新型冠状病毒感染肺炎病原体，具有下列情形之一，危害公共安全的，依照刑法第一百一十四条、第一百一十五条第一款的规定，以以危险方法危害公共安全罪定罪处罚：
1.已经确诊的新型冠状病毒感染肺炎病人、病原携带者，拒绝隔离治疗或者隔离期未满擅自脱离隔离治疗，并进入公共场所或者公共交通工具的；
2.新型冠状病毒感染肺炎疑似病人拒绝隔离治疗或者隔离期未满擅自脱离隔离治疗，并进入公共场所或者公共交通工具，造成新型冠状病毒传播的。

最高人民法院关于依法妥善审理高空抛物、坠物案件的意见
法发〔2019〕25号 2019年10月21日　见48页

最高人民法院、最高人民检察院、公安部关于依法惩治妨害公共交通工具安全驾驶违法犯罪行为的指导意见
公通字〔2019〕1号　2019年1月8日　见48页

最高人民法院、最高人民检察院、公安部《关于办理暴力恐怖和宗教极端刑事案件适用法律若干问题的意见》
（2014年9月9日 公通字〔2014〕34号）
二、准确认定案件性质
组织、领导、参加恐怖活动组织，同时实施杀人、放火、爆炸、非法制造爆炸物、绑架、抢劫等犯罪的，以组织、领导、参加恐怖组织罪和故意杀人罪、放火罪、爆炸罪、非法制造爆炸物罪、绑架罪、抢劫罪等数罪并罚。

最高人民检察院 公安部关于公安机关管辖的刑事案件立案追诉标准的规定（一）
公通字〔2008〕36号  2008年6月25日
第一条［失火案（刑法第一百一十五条第二款）］过失引起火灾，涉嫌下列情形之一的，应予立案追诉：
（一）造成死亡一人以上，或者重伤三人以上的；
（二）造成公共财产或者他人财产直接经济损失五十万元以上的；
（三）造成十户以上家庭的房屋以及其他基本生活资料烧毁的；
（四）造成森林火灾，过火有林地面积二公顷以上，或者过火疏林地、灌木林地、未成林地、苗圃地面积四公顷以上的；
（五）其他造成严重后果的情形。
本条和本规定第十五条规定的“有林地”、“疏林地”、“灌木林地”、“未成林地”、“苗圃地”，按照国家林业主管部门的有关规定确定。

最高人民法院关于醉酒驾车犯罪法律适用问题的意见
(2009年9月11日 法发〔2009〕47号)
为依法严肃处理醉酒驾车犯罪案件，统一法律适用标准，充分发挥刑罚惩治和预防犯罪的功能，有效遏制酒后和醉酒驾车犯罪的多发、高发态势，切实维护广大人民群众的生命健康安全，有必要对醉酒驾车犯罪法律适用问题作出统一规范。
一、准确适用法律，依法严惩醉酒驾车犯罪
刑法规定，醉酒的人犯罪，应当负刑事责任。行为人明知酒后驾车违法、醉酒驾车会危害公共安全，却无视法律醉酒驾车，特别是在肇事后继续驾车冲撞，造成重大伤亡，说明行为人主观上对持续发生的危害结果持放任态度，具有危害公共安全的故意。对此类醉酒驾车造成重大伤亡的，应依法以以危险方法危害公共安全罪定罪。
…
二、贯彻宽严相济刑事政策，适当裁量刑罚
根据刑法第一百一十五条第一款的规定，醉酒驾车，放任危害结果发生，造成重大伤亡事故，构成以危险方法危害公共安全罪的，应处以十年以上有期徒刑、无期徒刑或者死刑。具体决定对被告人的刑罚时，要综合考虑此类犯罪的性质、被告人的犯罪情节、危害后果及其主观恶性、人身危险性。一般情况下，醉酒驾车构成本罪的，行为人在主观上并不希望、也不追求危害结果的发生，属于间接故意犯罪，行为的主观恶性与以制造事端为目的而恶意驾车撞人并造成重大伤亡后果的直接故意犯罪有所不同，因此，在决定刑罚时，也应当有所区别。此外，醉酒状态下驾车，行为人的辨认和控制能力实际有所减弱，量刑时也应酌情考虑。
…
三、统一法律适用，充分发挥司法审判职能作用
为依法严肃处理醉酒驾车犯罪案件，遏制酒后和醉酒驾车对公共安全造成的严重危害，警示、教育潜在违规驾驶人员，今后，对醉酒驾车，放任危害结果的发生，造成重大伤亡的，一律按照本意见规定，并参照附发的典型案例，依法以以危险方法危害公共安全罪定罪量刑。
为维护生效裁判的既判力，稳定社会关系，对于此前已经处理过的将特定情形的醉酒驾车认定为交通肇事罪的案件，应维持终审裁判，不再变动。

最高人民法院、最高人民检察院关于办理、妨害预防、控制突发传染病疫情等灾害的刑事案件具体应用法律若干问题的解释
（2003 年5 月15 日起施行  法释〔2003〕8号）
第一条 故意传播突发传染病病原体，危害公共安全的，依照刑法第一百一十四条、第一百一十五条第一款的规定，按照以危险方法危害公共安全罪定罪处罚。
患有突发传染病或者疑似突发传染病而拒绝接受检疫、强制隔离或者治疗，过失造成传染病传播，情节严重，危害公共安全的，依照刑法第一百一十五条第二款的规定，按照过失以危险方法危害公共安全罪定罪处罚。

